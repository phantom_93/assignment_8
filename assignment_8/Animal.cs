﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_8
{
    #region enum Gender 
    public enum Gender
    {
        Male,
        Female
    }
    #endregion 
    public abstract class Animal
    {
        #region Properties 
        // properties 
        public Gender Gender { get; set; }
        public string Name { get; set; }

        public int Age { get; set; }
        public double Weight { get; set; }

        public double Height { get; set; }
        #endregion


        #region constructors 
        // constructors 

        public Animal(string name, int age, double weight)
        {
            Name = name;
            Age = age;
            Weight = weight;
            Gender = Gender.Male;
        }

        public Animal(string name, int age, double weight, double height, Gender gender)
        {
            Name = name;
            Age = age;
            Weight = weight;
            Gender = gender;
            Height = height;
        }

        #endregion
        // behaviours (methods) 

        #region methods
        public virtual void Hop(double distanceM)
        {
            Console.WriteLine($"{Name} can hop {distanceM} m away\n");
        }


        public virtual void MakeNoise(string sound)
        {
            Console.WriteLine($"{Name} makes a sound: {sound}\n");
        }

        public virtual void Sleep()
        {
            Console.WriteLine($"{Name} has gone to bed. Zzzzzz\n");
        }

        public abstract void Hunt();

        public virtual void Print()
        {
            Console.WriteLine($"{Name} is a {Gender} and {Age} years old, and weigh {Weight}kg and {Height}feet long \n");

        }

        #endregion

    }
}
