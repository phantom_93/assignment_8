﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_8
{
    #region Class Tiger
    public class Tiger : Animal, ISwimmer,IYawner, IEater
    {
        public bool Striped { get; set; }

        public string Skin { get; set; }
        // constructor 


        public Tiger(string name, int age, double weight, double height, Gender gender) : base(name, age, weight, height, gender)
        {
            Striped = false;
        }

        public override void Hunt()
        {
            Console.WriteLine($"When the {Name} is close enough, it springs from the cover, killing the prey with a bite to neck\n");
        }
        public override void Hop(double distanceM)
        {
            Console.WriteLine($"{Name} can hop {distanceM} m away\n");
        }


        public override void MakeNoise(string sound)
        {
            Console.WriteLine($"{Name} makes a sound: {sound}\n");
        }

        public override void Sleep()
        {
            Console.WriteLine($"{Name} has gone to bed. Zzzzzz\n");
        }

        public override void Print()
        {
            Console.WriteLine($"{Name} is a {Gender} and {Age} years old, and weigh {Weight}kg and {Height}feet long and it has the skin of {Skin} color and black {(Striped == true ? "stripes " : "")} over body \n");

        }

        public void Yawn()
        {
            Console.WriteLine($"{Name} is Yawning");
        }
        public void Swim()
        {
            Console.WriteLine($"{Name} loves swimming");
        }

        public void Eat()
        {

            Console.WriteLine($"{Name} eat the meat of the prey that it kills");
        }
    }
    #endregion


    #region sub-Tiger Species 
    // 4 different types of tiger species  
    public class SiberianTiger : Tiger
    {

        public SiberianTiger(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Striped = true;
            Skin = skin;
        }

    }

    public class SumatranTiger : Tiger
    {
        public SumatranTiger(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Striped = true;
            Skin = skin;
        }
    }

    public class WhiteBengalTiger : Tiger
    {
        public WhiteBengalTiger(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Striped = true;
            Skin = skin;
        }
    }


    public class MalayanTiger : Tiger
    {
        public MalayanTiger(string name, int age, double weight, double height, Gender gender, string skin) : base(name, age, weight, height, gender)
        {
            Striped = true;
            Skin = skin;
        }
    }
    #endregion 

}
