﻿using System;
using System.Collections.Generic;
namespace assignment_8
{
    class Program
    {
        static List<Animal> animals;

        static void Main(string[] args)
        {
            #region Create Tiger Objects
            // establish 4 tiger objects 
            WhiteBengalTiger sheerKhan = new WhiteBengalTiger("Sheer Khan", 5, 200, 12, Gender.Male, "white");
            SiberianTiger nala = new SiberianTiger("Nala", 6, 150, 10, Gender.Female, "bright orange");
            SumatranTiger ariel = new SumatranTiger("Ariel", 4, 180, 11, Gender.Female, "light orange");
            MalayanTiger catrina = new MalayanTiger("Catrina", 7, 140, 9, Gender.Female, "dark orange");
            #endregion


            #region Create Lion Objects
            //establish 4 Lions objects 

            WhiteLion simba = new WhiteLion("Simba", 6, 180, 7, Gender.Male, "white");
            KatangaLion sabrina = new KatangaLion("Sabrina", 5, 120, 7, Gender.Male, "reddish yellow");
            BarbaryLion nova = new BarbaryLion("Nova", 7, 130, 5, Gender.Male, "dark yellow");
            TransvaalLion aurora = new TransvaalLion("Aurora", 4, 140, 6.5, Gender.Male, "light yellow");
            #endregion

            #region Create Wolf Objects
            // establish 4 wolves objects 
            GrayWolf destiny = new GrayWolf("Destiny", 5, 55, 6, Gender.Female, "gray");
            GrayWolf alpha = new GrayWolf("Alpha", 5, 60, 6, Gender.Male, "gray-white");
            GrayWolf apollo = new GrayWolf("Apollo", 7, 54, 6, Gender.Male, "lightgray");
            GrayWolf asher = new GrayWolf("Asher", 6, 52, 6, Gender.Male, "whitesmoke");
            #endregion

            #region Create animalList 
            // create animalList and add created animal ojects to it 
            animals = new List<Animal>
            {
                sheerKhan,
                nala,
                ariel,
                catrina,
                simba,
                sabrina,
                nova,
                aurora,
                destiny,
                alpha,
                apollo,
                asher
            };
            #endregion

            #region Hop Method
            //hop functions 
            sheerKhan.Hop(8.5);
            nala.Hop(9.5);
            ariel.Hop(7.5);
            catrina.Hop(8.0);
            simba.Hop(14.5);
            sabrina.Hop(12.0);
            nova.Hop(11.0);
            aurora.Hop(11.5);
            destiny.Hop(4.0);
            alpha.Hop(3.5);
            apollo.Hop(3.6);
            asher.Hop(3.7);
            #endregion

            #region MakeNoise Method 
            // MakeNoise functions
            sheerKhan.MakeNoise("grrr, raaa");
            nala.MakeNoise("grrr, raaa");
            ariel.MakeNoise("grrr, raaa");
            catrina.MakeNoise("grrr, raaa");
            simba.MakeNoise("grauuw");
            sabrina.MakeNoise("grauuw");
            nova.MakeNoise("grauuw");
            aurora.MakeNoise("grauuw");
            destiny.MakeNoise("ou ou ouooooo");
            alpha.MakeNoise("ou ou ouooooo");
            apollo.MakeNoise("ou ou ouooooo");
            asher.MakeNoise("ou ou ouooooo");
            #endregion

            #region print, hunt, sleep methods
            foreach (Animal animal in animals)
            {
                animal.Print();
                animal.Hunt();
                animal.Sleep();
                

            }
            #endregion

            #region call InterfaceMethods
            Yawn();
            Console.WriteLine("\n\n");
            ISwimmer();
            Console.WriteLine("\n\n");
            Eat();
            #endregion
        }

        #region Interface Methods 
        private static void Eat()
        {
            foreach (IEater animal in animals)
            {
                animal.Eat();
            }
        }

        private static void ISwimmer()
        {
            foreach (ISwimmer animal in animals)
            {
                animal.Swim();
            }
        }

        private static void Yawn()
        {
            foreach (IYawner animal in animals)
            {
                animal.Yawn();
            }
        }

        #endregion
    }
}
