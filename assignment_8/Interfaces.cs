﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_8
{
    #region Interfaces
    // Create some interfaces for behaviour
    // yawning, climbing
    public interface IYawner
    {
        void Yawn();
    }

    public interface IClimber
    {
        void Climb();
    }

    public interface ISwimmer
    {
        void Swim();
    }

    public interface IKiller
    {
        void Kill();
    }

    public interface IEater
    {
        void Eat();
    }

    public interface ISleeper
    {
        void Sleep();
    }
    #endregion
}
