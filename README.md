
Assignment 8 - Zoology 3.0
--------------------------------------------------------------
Upgrade the Zoology application which manage a group of animals.

Use interfaces to assign movement types onto animal subtypes
Hint: IFlyer, ISwimmer or IClimber
Weight: Necessary